import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public numero_filas : any = [];
  public numero_colum : any = [];
  public posicionIcono: any = [];
  public colorPosicion: any = [];
  public colorClon: any = [];
  public asignarPosicion: any = [];
  public clonPosicion: any = [];
  public seguir : boolean = true;
  public cantidad : any = 20;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {
    for (let i = 0; i < this.cantidad; i++) {
      this.numero_filas.push(i);
      this.numero_colum.push(i);
      this.colorPosicion.push([]);
      this.colorClon.push([]);
      this.asignarPosicion.push([]);
      for (let j = 0; j < this.cantidad; j++) {
        this.colorPosicion[i][j] = "dark"; 
        this.asignarPosicion[i][j] = true; 
      }
    }
    this.clonPosicion = this.asignarPosicion;
    this.colorClon = this.colorPosicion;

  }

  colocarVida(fantasma : any)
  {
    let posicionArray = fantasma.split("-");
    let fila = posicionArray[0];
    let columna = posicionArray[1];
    if (this.asignarPosicion[fila][columna] )
    {
      this.colorPosicion[fila][columna] = "light"; 
      this.asignarPosicion[fila][columna] = false; 
    }
    else
    {
      this.colorPosicion[fila][columna] = "dark"; 
      this.asignarPosicion[fila][columna] = true; 
    }
    this.colorClon = this.colorPosicion;
    this.clonPosicion = this.asignarPosicion;
  }
  iniciar()
  {
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase',
      message: 'Do you want to buy this book?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Iniciar',
          handler: () => {
            this.iniciarCiclo();
          }
        }
      ]
    });
    alert.present();
  }

  iniciarCiclo() {
    let cantidad = this.cantidad;
    this.seguir = !this.seguir;
    let vivos = 0;
    let inter = 1;
    let timerId = setInterval(function () {
      inter++;
      for (let i = 0; i < cantidad; i++) {
        /*for (let j = 0; j < cantidad; j++)
        {
          console.log(i +"-", j)
          vivos = 0;
          if ((i - 1) >= 0) //Se verifican las casillas
          {
            if ((j - 1) >= 0)//Se verifica que la casilla este viva o muerta
            {
              if (this.asignarPosicion[i - 1][j - 1]) {
                vivos++;
              }
            }
            if (this.asignarPosicion[i - 1][j]) {
              vivos++;
            }
            if ((j + 1) < cantidad)//Se verifica que la casilla este viva o muerta
            {
              if (this.asignarPosicion[i - 1][j + 1]) {
                vivos++;
              }
            }

          }
          if ((j - 1) >= 0) {
            if (this.asignarPosicion[i][j - 1]) {
              vivos++;
            }
          }
          if ((j + 1) < cantidad) {
            if (this.asignarPosicion[i][j + 1]) {
              vivos++;
            }
          }
          if ((i + 1) < this.cantidad) //Se verifican las casillas
          {
            if ((j - 1) >= 0)//Se verifica que la casilla este viva o muerta
            {
              if (this.asignarPosicion[i + 1][j - 1]) {
                vivos++;
              }
            }
            if (this.asignarPosicion[i + 1][j]) {
              vivos++;
            }
            if ((j + 1) < cantidad)//Se verifica que la casilla este viva o muerta
            {
              if (this.asignarPosicion[i + 1][j + 1]) {
                vivos++;
              }
            }

          }
          //Saber si está viva
          if (this.asignarPosicion[i][j])
          {
            //Saber si hay vecinos
            switch (vivos) {
              case 2:
                this.clonPosicion[i][j] = true;
                break;
              case 3:
                this.clonPosicion[i][j] = true;
                break;
              default:
                this.clonPosicion[i][j] = false;
               this.colorClon[i][j] = "dark";
                break;
            }            
          }
          else
          {
            if (vivos == 3)
            {
              this.clonPosicion[i][j]= true;
              this.colorClon[i][j] = "light";
            }

          }
        }*/
      }
      if (inter == 10) {
        clearInterval(timerId);
      }
      this.asignarPosicion = this.clonPosicion;
      this.colorPosicion  = this.colorClon;
      
      console.log(inter)
    }, 1000);
  }

  

  
  
  /*seleccionarFantasma( fantasma : any)
  {
    this.asignar= true;
    for (let i = 0; i < 8; i++) {
      for(let j = 0; j< 5; j++)
      {
        if (this.posicionDisabled[i][j])
        {
          this.posicionShow[i][j] = true;
        }
      }
    }
    this.cantidad_utilizada = 0;
    switch (fantasma) {
      case 1:
        this.fantasmasColocado++;
        this.fantasmaDisabled[0] = false;
        this.fantasmaDisabled[1] = true;
        this.fantasmaDisabled[2] = true;
        this.fantasma = 0;
        break;
      case 2:
        this.fantasmaDisabled[0] = true;
        this.fantasmaDisabled[1] = false;
        this.fantasmaDisabled[2] = true;
        this.fantasma = 1;
        break;
      case 3:
        this.fantasmaDisabled[0] = true;
        this.fantasmaDisabled[1] = true;
        this.fantasmaDisabled[2] = false;
        this.fantasma = 2;
        break;
    }
  }
  colocarFantasma( posicion : any)
  {
    if (this.asignar)
    {
      let colocar = false;
      let pos = posicion.split("-");
      pos[0] = parseInt(pos[0]);
      pos[1] = parseInt(pos[1]);
      let color = "";
      this.cantidad_utilizada++;
      switch (this.fantasma) {
        case 0:
          //Se oculta y deshabilita el fantasma 1,
          this.fantasmaDisabled[0] = true;
          this.fantasmaShow[0] = true;
          color = "yellow";
          this.fantasmaDisabled[1] = false;
          this.fantasmaDisabled[2] = false;
          for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 5; j++) {
              this.posicionShow[i][j] = false;
            }
          }
          this.asignar = false;
          colocar = true;
          break; 
        case 1:
        let seguir = 0;
          if (this.cantidad_utilizada == 1 )
          {
            for (let i = 0; i < 8; i++) {
              for (let j = 0; j < 5; j++) {
                if (!this.posicionDisabled[i][j] && !this.posicionShow[i][j]) {
                  this.posicionShow[i][j] = true;
                }
              }
            }
            if (pos[0]+1 < 8)
            {
              if (!this.posicionDisabled[pos[0] + 1][pos[1]])
              {
                seguir++;
                this.posicionShow[pos[0] + 1][pos[1]] = false;
              }
            }
            if (pos[0] - 1 >= 0)
            {
              if (!this.posicionDisabled[pos[0] - 1][pos[1]])
              {
                seguir++;
                this.posicionShow[pos[0] - 1][pos[1]] = false;
              }
            }
            if (pos[1] + 1 < 5)
            {
              if (!this.posicionDisabled[pos[0]][pos[1] + 1])
              {
                seguir++;
                this.posicionShow[pos[0]][pos[1] + 1] = false;
              }
            }
            if (pos[1] - 1 >= 0)
            {
              if (!this.posicionDisabled[pos[0]][pos[1] - 1])
              {
                seguir++;
                this.posicionShow[pos[0]][pos[1] - 1] = false;
              }
            }
            if(seguir>0)
            {
              colocar = true;
            }

            color = "blue";
          }else {
            this.fantasmasColocado++;
            this.fantasmaDisabled[1] = true;
            this.fantasmaShow[1] = true;
            color = "blue";
            this.fantasmaDisabled[0] = false;
            this.fantasmaDisabled[2] = false;
            for (let i = 0; i < 8; i++) {
              for (let j = 0; j < 5; j++) {
                this.posicionShow[i][j] = false;
              }
            }
            this.asignar = false;
            colocar = true;
          }
          break;
        case 2:
        
          color = "green";
          switch (this.cantidad_utilizada) {
            case 1:
              for (let i = 0; i < 8; i++) {
                for (let j = 0; j < 5; j++) {
                  if (!this.posicionDisabled[i][j] && !this.posicionShow[i][j]) {
                    this.posicionShow[i][j] = true;
                  }
                }
              }
              let seguir = 0;
              if (pos[0] + 2 <= 8) {
                if ((!this.posicionDisabled[pos[0] + 1][pos[1]]) && (!this.posicionDisabled[pos[0] + 2][pos[1]])) {
                  seguir++;
                  this.posicionShow[pos[0] + 1][pos[1]] = false;
                }
              }
              if (pos[0] - 2 >= 0) {
                if ((!this.posicionDisabled[pos[0] - 1][pos[1]]) && (!this.posicionDisabled[pos[0] - 2][pos[1]])) {
                  seguir++;
                  this.posicionShow[pos[0] - 1][pos[1]] = false;
                }
              }
              if (pos[1] + 2 < 5) {
                if ((!this.posicionDisabled[pos[0]][pos[1] + 1]) && (!this.posicionDisabled[pos[0]][pos[1] + 2])) {
                  seguir++;
                  this.posicionShow[pos[0]][pos[1] + 1] = false;
                }
              }
              if (pos[1] - 2 >= 0) {
                if ((!this.posicionDisabled[pos[0]][pos[1] - 1]) && (!this.posicionDisabled[pos[0]][pos[1] - 2])) {
                  seguir++;
                  this.posicionShow[pos[0]][pos[1] - 1] = false;
                }
              }

              if (seguir > 0) {
                colocar = true;
              }
              else
              {
                for (let i = 0; i < 8; i++) {
                  for (let j = 0; j < 5; j++) {
                    this.posicionShow[i][j] = false;
                  }
                }
              }
              break; 
            case 2:
            let origeni = 0;
            let origenj = 0;
              for (let i = 0; i < 8; i++) {
                for (let j = 0; j < 5; j++) {
                  this.posicionShow[i][j] = true;
                  if (this.posicionColor[i][j] == "green")
                  {
                    origeni = i;
                    origenj = j;
                  }
                } 
              }
              if (origeni == pos[0])
              {
                if (pos[1] > origenj)
                {
                  this.posicionShow[pos[0]][pos[1] + 1] = false;
                }
                else {
                  this.posicionShow[pos[0]][pos[1] - 1] = false;
                }
              }else if (origenj == pos[1])
              {
                if (pos[0] > origeni)
                {
                  this.posicionShow[pos[0]+1][pos[1]] = false;
                }else {
                  this.posicionShow[pos[0]-1][pos[1]] = false;
                }
              }
              this.posicionShow[origeni][origenj] = false;
              colocar = true;
              break;
            case 3:
              this.fantasmasColocado++;
              this.fantasmaDisabled[0] = false;
              this.fantasmaDisabled[1] = false;
              this.fantasmaDisabled[2] = true;
              this.fantasmaShow[2] = true;
              this.asignar = false;
              colocar = true;
              for (let i = 0; i < 8; i++) {
                for (let j = 0; j < 5; j++) {
                  this.posicionShow[i][j] = false;
                }
              }
              break;
          }
          break;
      }
      if (colocar)
      {
        this.posicionShow[pos[0]][pos[1]] = false;
        this.posicionDisabled[pos[0]][pos[1]] = true;
        this.posicionColor[pos[0]][pos[1]] = color;
        this.posicionIcono[pos[0]][pos[1]] = "logo-snapchat";
      }
      else
      {
        alert("No se puede colocar fantasma en esta casilla");
        this.cantidad_utilizada--;
      }
      if (this.fantasmasColocado == 3) {
        this.atacarBarcos();
      }
    }
  }
  atacarBarcos()
  {
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 5; j++) {
          this.posicionShow[i][j] = false;
        }
    }
    let cont = 0;
    for(let i = 0; i<40; i++)
    {
      let x = this.obtenerX();
      let y = this.obtenerY();
      if (this.posicionDisabled[x][y] && this.posicionColor[x][y] != "red") {
        this.posicionColor[x][y] = "red";
        cont++;
      }
      else if (this.posicionColor[x][y] != "red") {
          this.posicionColor[x][y] = "grey";
      }
    }
      for (let i = 0; i < 8; i++) {
          for (let j = 0; j < 5; j++) {
              this.posicionDisabled[i][j] = true;
          }
      }
    if(cont == 6)
    {
      alert("Te ha ganado la computadora")
    }
    else
    {
      alert("Felicidades, le has ganado a la computadora")
    }
  }
  obtenerX()
  {
      this.x = (this.x * (new Date).getHours())%5;
      return this.x;
  }
  obtenerY()
  {
      this.y = (this.y * (new Date).getHours()) % 8;
      return this.y;
  }
  reiniciar() {
      
      for (let i = 0; i < 8; i++) {
          //Todas las de posicion
          this.posicion[i] = [];          //Array con todas las posiciones
          this.posicionDisabled[i] = [];  //Si es que las posiciones est�n deshabilitadas
          this.posicionShow[i] = [];      //Se es que las posiciones est�n visibles
          this.posicionColor[i] = [];     //Color de las posiciones
          this.posicionIcono[i] = [];     //Icono de las posiciones

          //Los fantasmas
          this.fantasmaDisabled[i] = false;
          this.fantasmaShow[i] = false;

          for (let j = 0; j < 5; j++) {
              this.posicion[i][j] = false;
              this.posicionDisabled[i][j] = false;
              this.posicionShow[i][j] = false;
              this.posicionColor[i][j] = "skyblue";
              this.posicionIcono[i][j] = "ios-home-outline";//"logo-snapchat";
          }
      }
      this.asignar= false;
      this.fantasmasColocado= 0;
      this.cantidad_utilizada= 0;
      this.x= 5;
      this.y= 15;
  }*/
  
}
